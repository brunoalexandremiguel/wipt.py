#!/usr/bin/env python3

import datetime, os, time, sys, argparse

#This script must be run as root!
if not os.geteuid()==0:
    sys.exit('This script must be run as root!')

#Create arguments
parser = argparse.ArgumentParser(description='Automatization for image optimization under Wordpress. Make those images sweat the fat out!', prog='Wordpress Images Personal Trainer')
parser.add_argument('-y','--year', metavar='YYYY', help='Year. Must use four numeric digits (ex.: -y 2018)', required=False)
parser.add_argument('-m','--month', metavar='MM', help='Month. Must use two numeric digits, 01 to 12 (ex.: -m 06)', choices=['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'], required=False)
parser.add_argument('-v', '--version', action='version', help='Show program version', version='%(prog)s 0.0.1')
args = parser.parse_args()

#Get current month and year
date = datetime.date.today()
m=date.strftime('%m')
y=date.strftime('%Y')

#Validate arguments
if len(sys.argv) <= 1:

    wordpress = "/path/to/wordpress/wp-content/uploads/"+str(y)+"/"+str(m)+"/"
    optimize = 'find . -type f -regex ".*\.\(jpg\|jpeg\|JPG\)" | parallel jpegoptim --all-progressive -s -v -m90 -t {};  find . -type f -name "*.png" | parallel optipng -fix -o5 -strip all {}'

    if os.path.exists(wordpress):
        print("Starting...")
        time.sleep(2)
        os.system("cd "+wordpress+";"+optimize)
        print("All done now!")
    else:
        print("Directory doesn't exist.")

    sys.exit('')

elif not len(sys.argv) < 7:

    parser.print_help()
    print('One too many arguments\n')
    sys.exit('Easy there, argumentaholic')

elif args.year is None or args.month is None:

    parser.print_help()
    print('Either -m and/or -y was not passed but args were\n')
    sys.exit('You may need to eat less cheese')

elif len(str(args.year)) != 4 or not str(args.year).isdigit() or len(str(args.month)) != 2 or not str(args.month).isdigit():
    
    parser.print_help()
    print ("Error: -m/--month is followed by two numbers. Example: -m 01\nError: -y/--year is followed by four numbers. Example: -y 2018\n")
    sys.exit('Take it easy on those numbers')

#Set automatic paths and populate it with current month and year
wordpress = "/path/to/wordpress/wp-content/uploads/"+str(args.year)+"/"+str(args.month)+"/"

#Define command to optimize images
optimize = 'find . -type f -regex ".*\.\(jpg\|jpeg\|JPG\)" | parallel jpegoptim --all-progressive -s -v -m90 -t {};  find . -type f -name "*.png" | parallel optipng -fix -o5 -strip all {}'

if os.path.exists(wordpress):
    print("Starting...")
    time.sleep(2)
    os.system("cd "+wordpress+";"+optimize)
    print("All done now!")
else:
    print("Directory doesn't exist.")
