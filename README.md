# wipt.py

## Automatization for image optimization under Wordpress. Make those images sweat the fat out!

**wipt** stands for Wordpress Images Personal Trainer and it's a *Python* script to leverage the *jpegoptim* and *optipng* tools in a given *Wordpress* upload directory.

### Dependencies
- Python 3.5 (tested with this version in *Debian 9.5*)
- jpegoptim
- optipng
- GNU parallel

### How it works
There are only two arguments and they are not mandatory.

If no arguments are passed, it gets the current month and year, browses to the image upload directory matching the current month and year, and runs the image optimization tools.

It is mandatory, however, to use one argument when invoking the other. Example:
`python3 wipt.py -m 02 -y 2018`
Ommiting *-m* or *-y* will result in an error.

### Usage:
```man
Wordpress Images Personal Trainer [-h] [-y YYYY] [-m MM] [-v]

Automatization for image optimization under Wordpress. Make those images sweat
the fat out!

optional arguments:
    -h, --help            show this help message and exit
    -y YYYY, --year YYYY  Year. Must use four numeric digits (ex.: -y 2018)
    -m MM, --month MM     Month. Must use two numeric digits, 01 to 12 (ex.: -m 06)
    -v, --version         Show program version
```

### ToDo:
- [ ] create argument for specifing custom *Wordpress* directory
- [ ] allow changing jpegoptim and optipng arguments via wipt.py arguments (still deciding if it's a good idea)
- [ ] integrate other image optimization tools, like zopflipng